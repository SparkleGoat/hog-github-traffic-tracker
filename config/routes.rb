Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html

  root to: 'charts#show'
  #get 'charts/:weekClones', to: 'charts#weekClones'
  get 'charts/weekView', to: 'charts#weekView'
  get 'charts/monthView', to: 'charts#monthView'
  get 'charts/threeMonthView', to: 'charts#threeMonthView'
  get 'charts/sixMonthView', to: 'charts#sixMonthView'

  get 'charts/weekClones', to: 'charts#weekClone'
  get 'charts/monthClones', to: 'charts#monthClone'
  get 'charts/threeMonthClones', to: 'charts#threeMonthClone'
  get 'charts/sixMonthClones', to: 'charts#sixMonthClone'

end
