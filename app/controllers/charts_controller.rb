class ChartsController < ApplicationController

  def show
    db = SQLite3::Database.open "db/development.sqlite3"
    @weekChart = db.execute("select full_date,view_count,view_unique from charts where full_date > date('now','-7 days');")
    @monthChart = db.execute("select full_date,view_count,view_unique from charts where full_date > date('now','-30 days');")
    @threeMonthChart = db.execute("select full_date,view_count,view_unique from charts where full_date > date('now','-90 days');")
    @sixMonthChart = db.execute("select full_date,view_count,view_unique from charts where full_date > date('now','-180 days');")
    @sumPath = db.execute("select path,SUM(path_count) from pop_paths Group by path;");
    @sumReferrer = db.execute("select referrer,SUM(referrer_count) from referrers Group by referrer;");
  end

  def weekView
    db = SQLite3::Database.open "db/development.sqlite3"
    @weekChart = db.execute("select full_date,view_count,view_unique from charts where full_date > date('now','-7 days');")
    @monthChart = db.execute("select full_date,view_count,view_unique from charts where full_date > date('now','-30 days');")
    print @monthChart
    @threeMonthChart = db.execute("select full_date,view_count,view_unique from charts where full_date > date('now','-90 days');")
    @sixMonthChart = db.execute("select full_date,view_count,view_unique from charts where full_date > date('now','-180 days');")
    @sumPath = db.execute("select path,SUM(path_count) from pop_paths Group by path;");
    @sumReferrer = db.execute("select referrer,SUM(referrer_count) from referrers Group by referrer;");
  end

  def monthView
    db = SQLite3::Database.open "db/development.sqlite3"
    @weekChart = db.execute("select full_date,view_count,view_unique from charts where full_date > date('now','-7 days');")
    @monthChart = db.execute("select full_date,view_count,view_unique from charts where full_date > date('now','-30 days');")
    print @monthChart
    @threeMonthChart = db.execute("select full_date,view_count,view_unique from charts where full_date > date('now','-90 days');")
    @sixMonthChart = db.execute("select full_date,view_count,view_unique from charts where full_date > date('now','-180 days');")
    @sumPath = db.execute("select path,SUM(path_count) from pop_paths Group by path;");
    @sumReferrer = db.execute("select referrer,SUM(referrer_count) from referrers Group by referrer;");
  end

  def threeMonthView
    db = SQLite3::Database.open "db/development.sqlite3"
    @weekChart = db.execute("select full_date,view_count,view_unique from charts where full_date > date('now','-7 days');")
    @monthChart = db.execute("select full_date,view_count,view_unique from charts where full_date > date('now','-30 days');")
    print @monthChart
    @threeMonthChart = db.execute("select full_date,view_count,view_unique from charts where full_date > date('now','-90 days');")
    @sixMonthChart = db.execute("select full_date,view_count,view_unique from charts where full_date > date('now','-180 days');")
    @sumPath = db.execute("select path,SUM(path_count) from pop_paths Group by path;");
    @sumReferrer = db.execute("select referrer,SUM(referrer_count) from referrers Group by referrer;");
  end

  def sixMonthView
    db = SQLite3::Database.open "db/development.sqlite3"
    @weekChart = db.execute("select full_date,view_count,view_unique from charts where full_date > date('now','-7 days');")
    @monthChart = db.execute("select full_date,view_count,view_unique from charts where full_date > date('now','-30 days');")
    print @monthChart
    @threeMonthChart = db.execute("select full_date,view_count,view_unique from charts where full_date > date('now','-90 days');")
    @sixMonthChart = db.execute("select full_date,view_count,view_unique from charts where full_date > date('now','-180 days');")
    @sumPath = db.execute("select path,SUM(path_count) from pop_paths Group by path;");
    @sumReferrer = db.execute("select referrer,SUM(referrer_count) from referrers Group by referrer;");
  end



  def weekClone
    db = SQLite3::Database.open "db/development.sqlite3"
    @weekClone = db.execute("select full_date,clone_count,clone_unique from clones where full_date > date('now','-7 days');")
    @monthClone = db.execute("select full_date,clone_count,clone_unique from clones where full_date > date('now','-30 days');")
    @threeMonthClone = db.execute("select full_date,clone_count,clone_unique from clones where full_date > date('now','-90 days');")
    @sixMonthClone = db.execute("select full_date,clone_count,clone_unique from clones where full_date > date('now','-180 days');")
    @sumPath = db.execute("select path,SUM(path_count) from pop_paths Group by path;");
    @sumReferrer = db.execute("select referrer,SUM(referrer_count) from referrers Group by referrer;");
  end

  def monthClone
    db = SQLite3::Database.open "db/development.sqlite3"
    @weekClone = db.execute("select full_date,clone_count,clone_unique from clones where full_date > date('now','-7 days');")
    @monthClone = db.execute("select full_date,clone_count,clone_unique from clones where full_date > date('now','-30 days');")
    @threeMonthClone = db.execute("select full_date,clone_count,clone_unique from clones where full_date > date('now','-90 days');")
    @sixMonthClone = db.execute("select full_date,clone_count,clone_unique from clones where full_date > date('now','-180 days');")
    @sumPath = db.execute("select path,SUM(path_count) from pop_paths Group by path;");
    @sumReferrer = db.execute("select referrer,SUM(referrer_count) from referrers Group by referrer;");
  end

  def threeMonthClone
    db = SQLite3::Database.open "db/development.sqlite3"
    @weekClone = db.execute("select full_date,clone_count,clone_unique from clones where full_date > date('now','-7 days');")
    @monthClone = db.execute("select full_date,clone_count,clone_unique from clones where full_date > date('now','-30 days');")
    @threeMonthClone = db.execute("select full_date,clone_count,clone_unique from clones where full_date > date('now','-90 days');")
    @sixMonthClone = db.execute("select full_date,clone_count,clone_unique from clones where full_date > date('now','-180 days');")
    @sumPath = db.execute("select path,SUM(path_count) from pop_paths Group by path;");
    @sumReferrer = db.execute("select referrer,SUM(referrer_count) from referrers Group by referrer;");
  end

  def sixMonthClone
    db = SQLite3::Database.open "db/development.sqlite3"
    @weekClone = db.execute("select full_date,clone_count,clone_unique from clones where full_date > date('now','-7 days');")
    @monthClone = db.execute("select full_date,clone_count,clone_unique from clones where full_date > date('now','-30 days');")
    @threeMonthClone = db.execute("select full_date,clone_count,clone_unique from clones where full_date > date('now','-90 days');")
    @sixMonthClone = db.execute("select full_date,clone_count,clone_unique from clones where full_date > date('now','-180 days');")
    @sumPath = db.execute("select path,SUM(path_count) from pop_paths Group by path;");
    @sumReferrer = db.execute("select referrer,SUM(referrer_count) from referrers Group by referrer;");
  end


end
