#!/usr/bin/env python

import argparse
import csv
from collections import OrderedDict
from datetime import date, timedelta
import getpass
import requests
import sqlite3


# Globals
#crontab -e
#* * 1 * * /your/command
#* * 15 * * /your/command
#service crond restart

current_timestamp = str( (date.today()).strftime('%Y-%m-%d'))

def get_paths(resource, auth, repo=None, headers=None):
    """Send request to Github API
    :param resource: string - specify the API to call
    :param auth: tuple - username-password tuple
    :param repo: string - if specified, the specific repository name
    :param headers: dict - if specified, the request headers
    :return: response - GET request response
    """
    
    username = 'KeyWCorp'
    if resource == 'traffic':
        # GET /repos/:owner/:repo/traffic/views <- from developer.github.com/v3/repos/traffic/#views
        base_url = 'https://api.github.com/repos/'
        base_url = base_url + username + '/' + repo + '/traffic/popular/paths'
        response = requests.get(base_url, auth=(username, auth), headers=headers)
        return response


def get_referrers(resource, auth, repo=None, headers=None):
    """Send request to Github API
    :param resource: string - specify the API to call
    :param auth: tuple - username-password tuple
    :param repo: string - if specified, the specific repository name
    :param headers: dict - if specified, the request headers
    :return: response - GET request response
    """
    username = 'KeyWCorp'
    if resource == 'traffic':
        # GET /repos/:owner/:repo/traffic/views <- from developer.github.com/v3/repos/traffic/#views
        base_url = 'https://api.github.com/repos/'
        base_url = base_url + username + '/' + repo + '/traffic/popular/referrers'
        response = requests.get(base_url, auth=(username, auth), headers=headers)
        return response


def timestamp_to_utc(timestamp):
    """Convert unix timestamp to UTC date
    :param timestamp: int - the unix timestamp integer
    :return: utc_data - the date in YYYY-MM-DD format
    """

    timestamp = int(str(timestamp)[0:10])
    utc_date = datetime.datetime.utcfromtimestamp(timestamp).strftime('%Y-%m-%d')

    return utc_date


def addPath(repo, json_response):
    """Parse traffic stats in JSON and format into a table
    :param repo: str - the GitHub repository name
    :param json_response: json - the json input
    :return: table: str - for printing on command line
    """
    repo_name = repo
    for each in json_response:
        add_to_db('(NULL,\'' +each['path'] +'\''+", "+ str(each['count']) + ", "+ str(each['uniques']) +',\'''\''+","+'\''+'\''+")", "pop_paths")

def add_to_db(item, tableName):
	#item is a string of data 
    conn = sqlite3.connect('stats/db/development.sqlite3')
    c = conn.cursor()
    c.execute('INSERT INTO ' + tableName +' VALUES ' + item)
    conn.commit()
    conn.close()
    #/Users/maryrogers/Downloads/stats/db/development.sqlite3


def main(repo):
    """Query the GitHub Traffic API
    :param repo: string - GitHub user's repo name or by default 'ALL' repos
    :return:
    """
    repo = repo.strip()
    traffic_headers = {'Accept': 'application/vnd.github.spiderman-preview'}
    token = 'dceffa029cbd1346dbb8eb4676d686768def949a' #special token for hog github

    traffic_response = get_referrers('traffic', token, repo, traffic_headers)
    traffic_response = traffic_response.json()
    addReferrers(repo, traffic_response)
    
    traffic_response = get_paths('traffic', token, repo, traffic_headers)
    traffic_response = traffic_response.json()
    addPath(repo, traffic_response)

    return ''



def addReferrers(repo, json_response):
    """Parse traffic stats in JSON and format into a table
    :param repo: str - the GitHub repository name
    :param json_response: json - the json input
    :return: table: str - for printing on command line
    """
    repo_name = repo
    for each in json_response:
        #print("Hello")
        add_to_db('(NULL,\'' + each['referrer'] +'\''+","+ str(each['count']) + ", "+ str(each['uniques'])+',\'''\''+","+'\''+'\''+")" , "referrers")

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('repo', help='User\'s repo')
    args = parser.parse_args()
    main(args.repo)
