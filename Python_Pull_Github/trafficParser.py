#!/usr/bin/env python

import argparse
import csv
from collections import OrderedDict
from datetime import date, timedelta
import getpass
import requests
import sqlite3

#crontab -e
#30 5 * * * /your/command
#service crond restart
# Globals
yesterday_timestamp = str( (date.today()- timedelta(1)).strftime('%Y-%m-%d'))

def get_views(resource, auth, repo=None, headers=None):
    """Send request to Github API
    :param resource: string - specify the API to call
    :param auth: tuple - username-password tuple
    :param repo: string - if specified, the specific repository name
    :param headers: dict - if specified, the request headers
    :return: response - GET request response
    """
    username = 'KeyWCorp'
    if resource == 'traffic':
        # GET /repos/:owner/:repo/traffic/views <- from developer.github.com/v3/repos/traffic/#views
        base_url = 'https://api.github.com/repos/'
        base_url = base_url + username + '/' + repo + '/traffic/views'
        response = requests.get(base_url, auth=(username, auth), headers=headers)
        return response

def get_clones(resource, auth, repo=None, headers=None):
    """Send request to Github API
    :param resource: string - specify the API to call
    :param auth: tuple - username-password tuple
    :param repo: string - if specified, the specific repository name
    :param headers: dict - if specified, the request headers
    :return: response - GET request response
    """
    username = 'KeyWCorp'
    if resource == 'traffic':
        # GET /repos/:owner/:repo/traffic/views <- from developer.github.com/v3/repos/traffic/#views
        base_url = 'https://api.github.com/repos/'
        base_url = base_url + username + '/' + repo + '/traffic/clones'
        response = requests.get(base_url, auth=(username, auth), headers=headers)
        #Sprint(response)
        return response

def timestamp_to_utc(timestamp):
    """Convert unix timestamp to UTC date
    :param timestamp: int - the unix timestamp integer
    :return: utc_data - the date in YYYY-MM-DD format
    """

    timestamp = int(str(timestamp)[0:10])
    utc_date = datetime.datetime.utcfromtimestamp(timestamp).strftime('%Y-%m-%d')
    return utc_date

def addViews(repo, json_response):
    """Parse traffic stats in JSON and format into a table
    :param repo: str - the GitHub repository name
    :param json_response: json - the json input
    :return: table: str - for printing on command line
    """
    
    repo_name = repo
    total_views = str(json_response['count'])
    total_uniques = str(json_response['uniques'])

    dates_and_views = OrderedDict()
    
    detailed_views = json_response['views']
    for row in detailed_views:
        utc_date = str(row['timestamp'][0:10])
        dates_and_views[utc_date] = (str(row['count']), str(row['uniques']))
    count = 0
    for row in dates_and_views:
        if (str(row.strip()) == str(yesterday_timestamp.strip())):
            add_to_db('(NULL,\'' +row.split("-")[0] +'\',\''+row.split("-")[1]+ '\',\''+ row.split("-")[2]+'\',\''+ row +'\'' + ","+dates_and_views[row][0] +","+ dates_and_views[row][1]+",\'' ,\'' "+')', "charts")
            count = count +1
    if count == 0:
        add_to_db('(NULL,\'' +str(yesterday_timestamp).split("-")[0] +'\',\''+str(yesterday_timestamp).split("-")[1]+ '\',\''+ str(yesterday_timestamp).split("-")[2]+'\',\''+ str(yesterday_timestamp) +'\'' + ","+ "0,0"+",\'' ,\'' "+')', "charts")



def addClones(repo, json_response):
    """Parse traffic stats in JSON and format into a table
    :param repo: str - the GitHub repository name
    :param json_response: json - the json input
    :return: table: str - for printing on command line
    """
    
    print(json_response)
    repo_name = repo
    total_views = str(json_response['count'])
    total_uniques = str(json_response['uniques'])

    dates_and_views = OrderedDict()
    
    detailed_views = json_response['clones']
    
    if len(detailed_views) == 0:
        add_to_db('(NULL,\'' +str(yesterday_timestamp).split("-")[0] +'\',\''+str(yesterday_timestamp).split("-")[1]+ '\',\''+ str(yesterday_timestamp).split("-")[2]+'\',\''+ str(yesterday_timestamp) +'\'' + ","+ "0,0"+",\'' ,\'' "+')' , "clones")
    else:
        for row in detailed_views:
        #print(row)
            utc_date = str(row['timestamp'][0:10])
            dates_and_views[utc_date] = (str(row['count']), str(row['uniques']))

        for row in dates_and_views:
            if (str(row.strip()) == str(yesterday_timestamp.strip())):
                add_to_db('(NULL,\'' +row.split("-")[0] +'\',\''+row.split("-")[1]+ '\',\''+ row.split("-")[2]+'\',\''+ row +'\'' + ","+dates_and_views[row][0] +","+ dates_and_views[row][1]+",\'' ,\'' "+')', "clones")


def add_to_db(item, tableName):
	#item is a string of data 
    conn = sqlite3.connect('stats/db/development.sqlite3')
    c = conn.cursor()
    c.execute('INSERT INTO ' + tableName +' VALUES ' + item)
    conn.commit()
    conn.close()
    #/Users/maryrogers/Downloads/stats/db/development.sqlite3

def main(repo):
    """Query the GitHub Traffic API
    :param repo: string - GitHub user's repo name or by default 'ALL' repos
    :return:
    """
    repo = repo.strip()
    traffic_headers = {'Accept': 'application/vnd.github.spiderman-preview'}
    token = 'dceffa029cbd1346dbb8eb4676d686768def949a' #special token for hog github
    
    traffic_response = get_views('traffic', token, repo, traffic_headers)
    traffic_response = traffic_response.json()
    addViews(repo, traffic_response)
    
    traffic_response = get_clones('traffic', token, repo, traffic_headers)
    traffic_response = traffic_response.json()
    addClones(repo, traffic_response)

    return ''


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('repo', help='User\'s repo')
    args = parser.parse_args()
    main(args.repo)
